package org.n1.mainframe.backend.model.ui

import org.n1.mainframe.backend.model.site.Site

class SiteInitState(
        val link: String,
        val id: Int,
        val site: Site
)