package org.n1.mainframe.backend.model.ui

data class NotyMessage(
        val type: String,
        val title: String,
        val message: String
)