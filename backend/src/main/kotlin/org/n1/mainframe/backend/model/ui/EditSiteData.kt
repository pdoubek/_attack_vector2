package org.n1.mainframe.backend.model.ui

data class EditSiteData (
        val siteId: String = "",
        val field: String = "",
        val value: String = "")