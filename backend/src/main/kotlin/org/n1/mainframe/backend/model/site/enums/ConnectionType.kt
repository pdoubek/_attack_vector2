package org.n1.mainframe.backend.model.site.enums

enum class ConnectionType {
    DEFAULT,
    UNSTABLE
}