package org.n1.mainframe.backend.model.site.enums

enum class ServiceType {

    OS,
    SYSCON,
    TEXT,
    FILE,
    TIME,
    CODE,
    RESOURCE,
    SCAN_BLOCKER,
    ICE_WORD_SEARCH,
    ICE_MAGIC_EYE,
    ICE_PASSWORD,
    ICE_MANUAL,
    ICE_UNHACKABLE

}